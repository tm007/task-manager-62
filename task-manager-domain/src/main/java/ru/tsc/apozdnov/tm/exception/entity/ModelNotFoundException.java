package ru.tsc.apozdnov.tm.exception.entity;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public final class ModelNotFoundException extends AbstractException {

    public ModelNotFoundException() {
        super("Error! Model not found...");
    }

}