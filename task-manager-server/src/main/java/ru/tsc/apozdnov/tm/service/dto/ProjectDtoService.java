package ru.tsc.apozdnov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.apozdnov.tm.api.service.dto.IProjectDtoService;
import ru.tsc.apozdnov.tm.dto.model.ProjectDtoModel;
import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.exception.field.EmptyDescriptionException;
import ru.tsc.apozdnov.tm.exception.field.EmptyIdException;
import ru.tsc.apozdnov.tm.exception.field.EmptyNameException;
import ru.tsc.apozdnov.tm.exception.field.EmptyUserIdException;
import ru.tsc.apozdnov.tm.exception.system.EmptyStatusException;
import ru.tsc.apozdnov.tm.repository.dto.ProjectDtoRepository;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;

@Service
public class ProjectDtoService extends AbstractDtoService<ProjectDtoModel> implements IProjectDtoService {

    @NotNull
    @Autowired
    private ProjectDtoRepository repository;

    @NotNull
    @Override
    protected ProjectDtoRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDtoModel create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable ProjectDtoModel project = new ProjectDtoModel();
        project.setName(name);
        project.setUserId(userId);

        repository.save(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDtoModel create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable ProjectDtoModel project = new ProjectDtoModel();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);

        repository.save(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDtoModel create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable ProjectDtoModel project = new ProjectDtoModel();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);

        repository.save(project);
        return project;
    }

    @Nullable
    @Override
    public ProjectDtoModel findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectDtoRepository repository = getRepository();
        return repository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectDtoRepository repository = getRepository();
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ProjectDtoRepository repository = getRepository();
        repository.deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<ProjectDtoModel> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ProjectDtoRepository repository = getRepository();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<ProjectDtoModel> findAll(@NotNull final String userId, @NotNull final Sort sort) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ProjectDtoRepository repository = getRepository();
        return repository.findAllByUserId(userId);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectDtoRepository repository = getRepository();
        return repository.existsByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public ProjectDtoModel findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectDtoRepository repository = getRepository();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @NotNull final ProjectDtoModel model) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ProjectDtoRepository repository = getRepository();
        repository.deleteByUserIdAndId(userId, model.getId());
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectDtoRepository repository = getRepository();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void update(@NotNull final ProjectDtoModel model) {
        @NotNull final ProjectDtoRepository repository = getRepository();
        repository.save(model);
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDtoModel updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final ProjectDtoRepository repository = getRepository();
        @Nullable final ProjectDtoModel model = repository.findFirstByUserIdAndId(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        repository.save(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDtoModel changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final ProjectDtoRepository repository = getRepository();
        @Nullable final ProjectDtoModel model = repository.findFirstByUserIdAndId(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        repository.save(model);
        return model;
    }

    @Override
    public long getCount(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ProjectDtoRepository repository = getRepository();
        return repository.countByUserId(userId);
    }

}